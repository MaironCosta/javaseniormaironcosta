package com.mobicare.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Setor")
public class Setor {

	@Id
	private String id;

	private String code;

	private String descricao;

	public Setor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Setor(String code, String descricao) {
		// TODO Auto-generated constructor stub
		this.code = code;
		this.descricao = descricao;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
