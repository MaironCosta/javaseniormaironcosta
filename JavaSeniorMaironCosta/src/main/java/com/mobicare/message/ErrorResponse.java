package com.mobicare.message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class ErrorResponse extends MessageResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Collection<String> erros = new ArrayList<String>();

	public ErrorResponse(Collection<String> erros, String description) {
		super(description, StatusMessage.ERROR);

		this.erros = erros;
	}

	public Collection<String> getErros() {
		return erros;
	}

}
