package com.mobicare.message;

import java.io.Serializable;

public class NonErrorResponse extends MessageResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NonErrorResponse(String description, StatusMessage statusMessage) {
		super(description, statusMessage);

	}

}
