package com.mobicare.message;

import java.io.Serializable;

import com.google.gson.Gson;

public abstract class MessageResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String description;
	
	private String statusDescription;
	
	private String statusCode;

	public MessageResponse(String description, StatusMessage statusMessage) {
		
		this.description = description;
		this.statusCode = statusMessage.getCode();
		this.statusDescription = statusMessage.getDescription();
		
	}

	public String getDescription() {
		return description;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public String getStatusCode() {
		return statusCode;
	}
	
	public enum StatusMessage {
		
		SUCCESS ("1", "Sucesso."),
		ERROR ("2", "Erro."),
		RESULT_NOT_FOUND("3", "Sem Resultado."),
		ACESSO_DENIED ("4", "Acesso Negado.");
		
		private String code, description;
		
		private StatusMessage (String code, String description) {
			this.code = code;
			this.description = description;
		}
		
		public String getCode() {
			return this.code;
		}
		
		public String getDescription() {
			return this.description;
		}
		
	}
	
	public String getJSON() {

		Gson gson = new Gson();
		String json = gson.toJson(this);
		
		return json;
	}

}
