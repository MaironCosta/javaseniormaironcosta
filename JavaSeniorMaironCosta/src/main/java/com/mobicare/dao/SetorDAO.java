package com.mobicare.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.mobicare.entity.Setor;

@Service
public interface SetorDAO extends MongoRepository<Setor, String>  {

	public Setor getByCode (String code);

}
