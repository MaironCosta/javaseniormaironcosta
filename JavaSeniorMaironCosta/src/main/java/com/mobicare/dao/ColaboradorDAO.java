package com.mobicare.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.mobicare.entity.Colaborador;

@Service
public interface ColaboradorDAO extends MongoRepository<Colaborador, String> {

	public Colaborador getByCpf (String cpf);

}
