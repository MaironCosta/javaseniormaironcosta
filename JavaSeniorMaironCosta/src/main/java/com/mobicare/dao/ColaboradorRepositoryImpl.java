package com.mobicare.dao;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.mobicare.entity.Colaborador;

@Service
public class ColaboradorRepositoryImpl {

	private Logger log = Logger.getLogger(this.getClass());
	
	private MongoTemplate mongoTemplate;

	@Autowired
	public ColaboradorRepositoryImpl(MongoTemplate mongoTemplate) {
		// TODO Auto-generated constructor stub
		this.mongoTemplate = mongoTemplate;
	}

	public Collection<Colaborador> listarAgrupadoPorSetor() {

		Query query = new Query();
		query.with(new Sort(Sort.Direction.ASC, "setor"));
		
		log.info("query: " + query);

		Collection<Colaborador> colaboradors = mongoTemplate.find(query, Colaborador.class);
		
		return colaboradors;
	}

}
