package com.mobicare.controller.v1;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.mobicare.business.ColaboradorBusiness;
import com.mobicare.dto.ColaboradoresSetorDTO;
import com.mobicare.entity.Colaborador;
import com.mobicare.exception.ValidationException;
import com.mobicare.message.ErrorResponse;
import com.mobicare.message.MessageResponse.StatusMessage;
import com.mobicare.message.NonErrorResponse;

@RestController
@RequestMapping(path = "colaborador/v1/", produces = "application/json")
public class ColaboradorController {

	private Gson gson = new Gson();
	
	private ColaboradorBusiness colaboradorBusiness;

	@Autowired
	public ColaboradorController(ColaboradorBusiness colaboradorBusiness) {
		// TODO Auto-generated constructor stub
		this.colaboradorBusiness = colaboradorBusiness;
	}

	@PostMapping(path = "inserir")
	public String inserir(Colaborador colaborador) {

		try {
			
			colaboradorBusiness.inserir(colaborador);
			
			String json = gson.toJson(colaborador);
			
			return json;
			
		} catch (ValidationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			ErrorResponse error = new ErrorResponse(e.getErros(), "Validation Exception");
			return error.getJSON();
		}
		 
	}

	@DeleteMapping(path = "remover")
	public String remover(String cpf) throws ValidationException {

		colaboradorBusiness.remover(cpf);

		NonErrorResponse response = new NonErrorResponse("Realizado com Sucesso.", StatusMessage.SUCCESS);
		String json = response.getJSON();

		return json;
	}

	@GetMapping(path = "buscar/{cpf}")
	public String buscar(@PathVariable(value = "cpf") String cpf) {

		Colaborador colaborador = colaboradorBusiness.buscar(cpf);
		
		String json = gson.toJson(colaborador);

		return json;
	}

//	public String buscar(ColaboradorFiltro colaboradorFiltro) {
//
//		String json = "";
//
//		return json;
//	}

	@GetMapping(path = "listar-agrupado-setor")
	public String listarAgrupadoPorSetor() {

		Collection<Colaborador> listarAgrupadoPorSetor = colaboradorBusiness.listarAgrupadoPorSetor();
		
		String json = gson.toJson(ColaboradoresSetorDTO.getInstance(listarAgrupadoPorSetor));

		return json;
	}

}
