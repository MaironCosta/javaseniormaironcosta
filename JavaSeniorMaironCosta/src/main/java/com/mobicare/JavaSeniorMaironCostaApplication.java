package com.mobicare;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mobicare.entity.Colaborador;
import com.mobicare.entity.Setor;

@SpringBootApplication
public class JavaSeniorMaironCostaApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSeniorMaironCostaApplication.class, args);
	}
	
	@Bean
	public boolean checarTabelas (MongoTemplate mongoTemplate) {

		boolean setorCollectionExists = mongoTemplate.collectionExists(Setor.class);
		boolean colaboradorCollectionExists = mongoTemplate.collectionExists(Colaborador.class);
		
		if (!colaboradorCollectionExists) {
			
			mongoTemplate.createCollection(Colaborador.class);
			
		}
		
		if (!setorCollectionExists) {
			
			mongoTemplate.createCollection(Setor.class);
			
			Collection<Setor> setores = new ArrayList<Setor>();
			setores.add(new Setor("1", "Desenvolvedor"));
			setores.add(new Setor("2", "Tesouraria"));
			setores.add(new Setor("3", "Secretaria"));
			
			mongoTemplate.insert(setores, Setor.class);
			
		}
		
		return true;
		
	}
}
