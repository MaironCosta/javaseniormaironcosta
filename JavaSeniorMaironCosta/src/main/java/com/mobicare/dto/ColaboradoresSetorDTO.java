package com.mobicare.dto;

import java.util.ArrayList;
import java.util.Collection;

import com.mobicare.entity.Colaborador;
import com.mobicare.utils.ValidacoesUtils;

public class ColaboradoresSetorDTO {
	
	private String nome, email;
	
	private ColaboradoresSetorDTO(String nome, String email) {
		// TODO Auto-generated constructor stub
		this.nome = nome;
		this.email = email;
	}
	
	public static Collection<ColaboradoresSetorDTO> getInstance (Collection<Colaborador> colaboradors) {
		
		if (ValidacoesUtils.isEmpty(colaboradors))
			return new ArrayList<ColaboradoresSetorDTO>();
		
		Collection<ColaboradoresSetorDTO> result = new ArrayList<ColaboradoresSetorDTO>();
		
		for (Colaborador colaborador : colaboradors) {

			ColaboradoresSetorDTO col = new ColaboradoresSetorDTO(colaborador.getNome(), 
					                                              colaborador.getEmail());
			
			result.add(col);
			
		}
		
		return result;
	}
	
	public static ColaboradoresSetorDTO getInstance (Colaborador colaborador) {

		if (colaborador == null)
			return new ColaboradoresSetorDTO("", "");
		
		ColaboradoresSetorDTO col = new ColaboradoresSetorDTO(colaborador.getNome(), 
				                                              colaborador.getEmail());
		return col;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
