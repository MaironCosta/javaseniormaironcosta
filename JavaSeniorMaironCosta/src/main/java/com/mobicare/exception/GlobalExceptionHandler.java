package com.mobicare.exception;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

@ControllerAdvice
public class GlobalExceptionHandler {

	public GlobalExceptionHandler() {
		// TODO Auto-generated constructor stub
	}

	@ExceptionHandler({ ValidationException.class})
	public final ResponseEntity<ValidationError> handleException(Exception ex, WebRequest request) {
		
		HttpHeaders headers = new HttpHeaders();

		if (ex instanceof ValidationException) {
			HttpStatus status = HttpStatus.METHOD_FAILURE;
			ValidationException unfe = (ValidationException) ex;

			return this.handleValidationException(unfe, headers, status, request);
			
		} else {
			
			HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
			return this.handleExceptionInternal(ex, null, headers, status, request);
			
		}
	}

//    /** Customize the response for ContentNotAllowedException. */
//    protected ResponseEntity<ValidationException> handleValidationException(ValidationException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
//        List<String> errorMessages = (List<String>) ex.getErros();
//
//        return handleExceptionInternal(ex, new ValidationException(errorMessages), headers, status, request);
//    }

    /** Customize the response for ContentNotAllowedException. */
    protected ResponseEntity<ValidationError> handleValidationException(ValidationException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
       
    	List<String> errorMessages = (List<String>) ex.getErros();

        return handleExceptionInternal(ex, new ValidationError(errorMessages), headers, status, request);
    }

    /** A single place to customize the response body of all Exception types. */
    protected ResponseEntity<ValidationError> handleExceptionInternal(Exception ex, ValidationError body, HttpHeaders headers, HttpStatus status, WebRequest request) {
     
    	if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        }

        return new ResponseEntity<>(body, headers, status);
    }
}
