package com.mobicare.exception;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ValidationException extends Exception {
	
//	Logger log = Logger.getLogger(this.getClass());

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Collection<String> erros = new ArrayList<String>();	

	public ValidationException(Collection<String> erros) {
		super();
		this.erros = erros;
	}
	
	public ValidationException(String erro) {
		super();
		this.erros.add(erro);
	}

	public Collection<String> getErros() {

//		for (String erro : erros) 				
//			log.info(erro);

		return erros;
	}
}
