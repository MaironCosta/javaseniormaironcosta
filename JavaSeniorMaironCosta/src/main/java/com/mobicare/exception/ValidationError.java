package com.mobicare.exception;

import java.util.ArrayList;
import java.util.Collection;

public class ValidationError {
	
//	Logger log = Logger.getLogger(this.getClass());

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Collection<String> erros = new ArrayList<String>();	

	public ValidationError(Collection<String> erros) {
		super();
		this.erros = erros;
	}
	
	public ValidationError(String erro) {
		super();
		this.erros.add(erro);
	}

	public Collection<String> getErros() {

		return erros;
	}
}
