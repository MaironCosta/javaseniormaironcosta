package com.mobicare.utils;

import java.util.Calendar;
import java.util.Collection;
import java.util.Map;
import java.util.TimeZone;

public class ValidacoesUtils {

	static TimeZone COUNTRY_TIMEZONE = TimeZone.getTimeZone("America/Sao_Paulo");		
	
	public static boolean isEmpty(Collection<?> list){
		
		if(list == null)
			return true;
		else if(list.isEmpty()) 
			return true;
		else if(list.size() < 1)
			return true;
		
		return false;
	}
	
	public static boolean isEmpty(String string){
		
		if(string == null)
			return true;
		else if (string.trim().equals(""))
			return true;
		else if (string.length() < 1)
			return true;
		
		return false;
	}
	
	public static boolean isEmpty(Map<?, ?> map){
		
		if(map == null)
			return true;
		else if (map.isEmpty())
			return true;
		else if (map.size() < 1)
			return true;
		
		return false;
	}
	
	public static boolean isEmpty(Calendar date){
		
		if(date == null)
			return true;

		return false;
	}

	public static boolean isEmpty(Object[] array) {

		if (array == null || array.length == 0)
			return true;

		return false;
	}
	
}