package com.mobicare.business;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobicare.dao.ColaboradorDAO;
import com.mobicare.dao.ColaboradorRepositoryImpl;
import com.mobicare.dao.SetorDAO;
import com.mobicare.entity.Colaborador;
import com.mobicare.entity.Setor;
import com.mobicare.exception.ValidationException;
import com.mobicare.filtro.ColaboradorFiltro;
import com.mobicare.utils.ValidacoesUtils;

@Service
public class ColaboradorBusiness {
	
	private ColaboradorDAO colaboradorDAO;
	
	private SetorDAO setorDAO;
	
	private ColaboradorRepositoryImpl colaboradorRepositoryImpl;

	@Autowired
	public ColaboradorBusiness(ColaboradorDAO colaboradorDAO, SetorDAO setorDAO, ColaboradorRepositoryImpl colaboradorRepositoryImpl) {
		// TODO Auto-generated constructor stub
		this.colaboradorDAO = colaboradorDAO;
		this.setorDAO = setorDAO;
		this.colaboradorRepositoryImpl = colaboradorRepositoryImpl;
	}

	public Colaborador inserir(Colaborador colaborador) throws ValidationException {

		this.validar(colaborador);
		
		colaboradorDAO.save(colaborador);
		
		return colaborador;
	}

	private void validar(Colaborador colaborador) throws ValidationException {

		if (colaborador == null)
			throw new ValidationException("Colaborador is null.");
		
		Collection<String> erros = new ArrayList<String>();
		
		if (ValidacoesUtils.isEmpty(colaborador.getCpf())) {
			
			erros.add("Por Favor, informe o cpf.");
			
		} else {
			
			Colaborador colaboradorByCPF = this.buscar(colaborador.getCpf());
			
			if (colaboradorByCPF != null) {
				
				erros.add("Colaborador com este CPF contido na base.");
				
			}
			
		}
		
		if (ValidacoesUtils.isEmpty(colaborador.getEmail())) {
			
			erros.add("Por Favor, informe o e-mail.");
			
		}
		
		if (ValidacoesUtils.isEmpty(colaborador.getNome())) {
			
			erros.add("Por Favor, informe o nome.");
			
		}
		
		if (ValidacoesUtils.isEmpty(colaborador.getTelefone())) {
			
			erros.add("Por Favor, informe o telefone.");
			
		}
		
		if (ValidacoesUtils.isEmpty(colaborador.getCodigoSetor())) {
			
			erros.add("Por Favor, informe o setor.");
			
		} else {
			
			// falta implementar a busca do setor.
			Setor setor = setorDAO.getByCode(colaborador.getCodigoSetor());
			
			if (setor == null) {
				
				erros.add("Por favor, informe o setor.");
				
			} else {
				
				colaborador.setSetor(setor);
				
			}
			
		}
		
		if (!ValidacoesUtils.isEmpty(erros)) {
			
			throw new ValidationException(erros);
			
		}
		
	}

	public void remover(Colaborador colaborador) {

		colaboradorDAO.delete(colaborador);
		
	}
	
	public void remover(String cpf) throws ValidationException {

		Colaborador colaborador = this.buscar(cpf);

		if (colaborador != null) 
			this.remover(colaborador);

		String msg = "Por favor, informe o cpf do colaborador a ser removido.";
		throw new ValidationException(msg);

	}

	public Colaborador buscar(String cpf) {

		Colaborador colaborador = colaboradorDAO.getByCpf(cpf);

		return colaborador;
	}
	
	public String buscar(ColaboradorFiltro colaboradorFiltro) {

		String json = "";

		return json;
	}

	public Collection<Colaborador> listarAgrupadoPorSetor() {

		Collection<Colaborador> agrupadoPorSetor = colaboradorRepositoryImpl.listarAgrupadoPorSetor();

		return agrupadoPorSetor;
	}
	
}
